/*
 * Physics.h integrates the Bullet Physics engine into the game
 * Author: Alex Flight
 * Version: 3.0
 * Date:  21/04/2014
 *  v2.0  20/01/2014
 *  v1.0  13/01/2014
 */

#include "Physics.h"

// Init all the Bullet/BtOgre Physics components. Physics is handled exclusively by Broadphase to simplify matters.
Physics::Physics()
{
	bulBroadphase = new btDbvtBroadphase();
	bulCollisionConfiguration = new btDefaultCollisionConfiguration();
	bulDispatcher = new btCollisionDispatcher(bulCollisionConfiguration);
	bulSolver = new btSequentialImpulseConstraintSolver;
	bulDynamicsWorld = new btDiscreteDynamicsWorld(bulDispatcher, bulBroadphase, bulSolver, bulCollisionConfiguration);
	// There's no gravity in space!
	bulDynamicsWorld->setGravity(btVector3(0,0,0));
	bulCollisionWorld = new btCollisionWorld(bulDispatcher, bulBroadphase, bulCollisionConfiguration);
	// Debug drawer for determining bounding boxes etc.
	bulDebugDrawer = new BtOgre::DebugDrawer(Engine::getSingletonPtr()->mSceneManager->getRootSceneNode(), bulDynamicsWorld);
}


Physics::~Physics()
{
	//delete bulDynamicsWorld;
    //delete bulSolver;
    //delete bulDispatcher;
    //delete bulCollisionConfiguration;
    //delete bulBroadphase;
}

// Add a RigidBody to the Phyisics worldspace
void Physics::addRigidBody(btRigidBody* body)
{
	bulDynamicsWorld->addRigidBody(body);
	body->setUserPointer(this);
}

// Add a CollisionObject to the Phyisics worldspace
void Physics::addCollisionObject(btCollisionObject* obj)
{
	bulCollisionWorld->addCollisionObject(obj);
}

// Step the Physics simulation by an amount of time and iterate over the passed number of steps assuemd to have occurred in that time-frame
void Physics::stepSimulation(double time, int subSteps)
{
	bulDynamicsWorld->stepSimulation(time, subSteps);
	//bulDebugDrawer->step();
}

void Physics::setupDebugDrawer()
{
	bulDynamicsWorld->setDebugDrawer(bulDebugDrawer);
}