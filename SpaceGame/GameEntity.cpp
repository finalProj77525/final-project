/*
 * GameEntity.cpp class represents every basic Entity within the game world with a mesh and SceneNode as well as methods for getting position, the SceneNode,
 * the Name and methods to destroy the object in the game.
 * Author : Alex Flight
 * Version : 1.0
 * Date : 17/11/2013
 */

#include "GameEntity.h"

// Init with basic variables and attributes including mesh, alive state, bool to declare if destroyable or not and attach everything to a SceneNode
GameEntity::GameEntity(Ogre::String eName, Ogre::String mName, bool isDestroyable)
{
	destroyable = isDestroyable;
	entityName = eName;
	meshName = mName;
	alive = true;
	sceneManager = Engine::getSingletonPtr()->mSceneManager;
	entity = sceneManager->createEntity(entityName, meshName);
	sceneNode = sceneManager->getRootSceneNode()->createChildSceneNode(entityName+"_Node");
	sceneNode->attachObject(entity);
	//sceneNode->setPosition(Ogre::Vector3(0, 0, -25));
}


GameEntity::~GameEntity()
{
	entity->detachFromParent();
	sceneNode->removeAndDestroyAllChildren();
}

Ogre::Vector3 GameEntity::getPosition()
{
	return sceneNode->getPosition();
}

Ogre::SceneNode* GameEntity::getSceneNode()
{
	return sceneNode;
}

Ogre::String GameEntity::getName() 
{
	return entity->getName();
}

void GameEntity::setPosition(Ogre::Vector3 pos)
{
	sceneNode->setPosition(pos);
}

// Attempt to destroy the Entity, if not destroyable return log error message
bool GameEntity::destroy()
{
	if(destroyable)
	{
		alive = false;
	} else
	{
		Engine::getSingletonPtr()->mLog->logMessage(this->entityName+"_Entity is not DESTROYABLE");
		return false;
	}
	return true;
}

bool GameEntity::isAlive()
{
	return alive;
}
