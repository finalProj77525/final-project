/*
 * Spacecraft.h class manages all Spacecraft Entities in the game world including gameplay logic, physics and movement
 * Author : Alex Flight
 * Version : 12.0
 * Date :   05/05/2014
 *	 v11.0  20/04/2014
 *   v10.0  05/04/2014
 *   v9.0   15/03/2014
 *   v8.0   12/03/2014
 *   v7.0   04/03/2014
 *   v6.0   18/02/2014
 *   v5.0   03/02/2014
 *   v4.0   20/01/2014
 *   v3.0   14/12/2013
 *   v2.0   28/11/2013
 *   v1.0   17/11/2013
 */

#ifndef SPACECRAFT_H
#define SPACECRAFT_H

#pragma once

#include "MoveableObject.h"
#include "Laser.h"

class MoveableObject; //Forward declaration

class Spacecraft : public MoveableObject
{
public:
	Spacecraft(Ogre::String entName, Ogre::String meshName, bool isDestroyable, float objMass, float maxVel, float maxAccel, float roll, float pitch, float yaw,
		float decceleration, float translate);
	~Spacecraft();

	float getShields();
	float getHull();
	
	void pitch(float amount);
	void roll(float amount);
	void yaw(float amount);
	void thrust(float amount);
	void translateX(float amount);
	void translateY(float amount);
	
	void applyDamage(float amount);
	void fireWeaponPrimary(Ogre::BillboardSet* laserSet, std::vector<Laser>* lasers, double timeSinceLastFrame);
	void fireWeaponSecondary();
	void isFiring(int toggle);
	bool getFiring();

	void update(double timeSinceLastFrame, Physics* physEngine);
	
	void setupPhysics();
	btRigidBody* getRigidBody();
	btVector3 getTranslationVector();
	btVector3 getRotationVector();

protected:
	btRigidBody* shipRigidBody;
	btCollisionObject* shipCollisionObj;
	btVector3 translation;
	btVector3 rotation;

	float ROLL_RATE;
    float YAW_RATE;
	float PITCH_RATE;
	float MAX_VELOCITY;
	float MAX_ACCELERATION;
	float DECCELERATION_RATE;
	float TRANSLATE_RATE;
	float MAX_SHIELDS;
	float MAX_HULL;

	float shields;
	float hull;

	float currPitch;
	float currYaw;
	float currRoll;
	float currTranslateX;
	float currTranslateY;
	float currTranslateZ;

	float laserCooldown;
	bool firing;

	std::vector<Laser*> lasers;
	int laserID;

	//Weapon* primaryWeapon;
	//Weapon secondaryWeapon; TODO
};

#endif