/*
 * MoveableObject.h class extends GameEntity with Physics features including a MotionState, CollisionShape and a RigidBody
 * Author : Alex Flight
 * Version : 2.0
 * Date : 05/03/2014
 *  v1.0  17/11/2013
 */

#include "MoveableObject.h"

MoveableObject::MoveableObject(Ogre::String entName, Ogre::String meshName, bool isDestroyable, float objMass)
	: GameEntity(entName, meshName, isDestroyable)
{
	// Create a rouge Rigid body sphere based on the Entity's mesh.
	MASS = objMass;
	motionState = new BtOgre::RigidBodyState(sceneNode);
	BtOgre::StaticMeshToShapeConverter converter(entity);
	shape = converter.createSphere();
}


MoveableObject::~MoveableObject()
{
}

BtOgre::RigidBodyState* MoveableObject::getMotionState()
{
	return motionState;
}