/*
 * Game.h intialises the GameStates and Engine resources
 * Author : Alex Flight
 * Version : 1.0
 * Date : 11/11/2013
 */
#include "Game.h"
#include "MenuState.h"
#include "GameState.h"
#include "PauseState.h"

Game::Game()
{
	mEngineStateManager = 0;
}

Game::~Game()
{
	delete mEngineStateManager;
	delete Engine::getSingletonPtr();
}

void Game::start()
{
	new Engine();
	if(!Engine::getSingletonPtr()->initOgreRenderer("SpaceGame Test", 0, 0))
		return;

	Engine::getSingletonPtr()->mLog->logMessage("SpaceGame initialized!");

	mEngineStateManager = new EngineStateManager();

	MenuState::create(mEngineStateManager, "MenuState");
	GameState::create(mEngineStateManager, "GameState");
	PauseState::create(mEngineStateManager, "PauseState");

	// Immediately enter MenuState
	mEngineStateManager->start(mEngineStateManager->findByName("MenuState"));
}
