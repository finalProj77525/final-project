/*
 * MoveableObject.h class extends GameEntity with Physics features including a MotionState, CollisionShape and a RigidBody
 * Author : Alex Flight
 * Version : 2.0
 * Date : 05/03/2014
 *  v1.0  17/11/2013
 */

#ifndef MOVEABLE_OBJECT_H
#define MOVEABLE_OBJECT_H

#pragma once

#include "GameEntity.h"

class GameEntity;

class MoveableObject : public GameEntity
{
public:
	MoveableObject(Ogre::String entName, Ogre::String meshName, bool isDestroyable, float objMass);
	~MoveableObject();

	BtOgre::RigidBodyState* getMotionState();
protected:
	float MASS;
	BtOgre::RigidBodyState* motionState;
	btCollisionShape* shape;
};

#endif