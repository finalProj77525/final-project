/*
 * Laser.h represents a Laser Billboard object that displays a laser projectile fired by the player. Additionally the Laser uses Ray Casts to determine hits
 * Author : Alex Flight
 * Version : 2.0
 * Date : 28/04/2014
 *  v1.0  15/03/2014
 */

#pragma once
#include "Engine.h"

class Laser
{
public:
	Laser(Ogre::BillboardSet* laserChain, Ogre::SceneNode* parent, std::vector<Laser> *laserList, int width, int dmg, float rate, float life, Ogre::String name);
	~Laser();

	void fire();
	const btCollisionObject* castRay(Physics* physEngine);
	bool update(float timeSinceLastFrame, Physics* physEngine);

protected:
	Ogre::Billboard* laserBillboard;
	Ogre::Vector3 direction, velocity;
	int LASERWIDTH;
	int LASERDMG;
	float lifetime;
};

