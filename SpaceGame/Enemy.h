/*
 * Enemy.h represents an non-player controlled Enemy spacecraft entity within the game world. The class extends Spacecraft and introduces
 * a waypoint patrol system, an AI state system with pursue, patrol and flee states, and a series of nodes to incorporate ribbon trails
 * to allow for easier spotting of the Enemy against the background using its engine trails.
 * Author : Alex Flight
 * Version : 2.0
 * Date : 15/04/2014
 *	v1.0  05/01/2014      
 */

#pragma once
#include "Spacecraft.h"
class Enemy : public Spacecraft
{
public:
	Enemy();
	Enemy(Ogre::String entName, Ogre::String meshName, bool isDestroyable, float objMass, float maxVel, float maxAccel, float roll, float pitch, float yaw,
		float decceleration, float translate);
	~Enemy();

	void setupEngineTrails();

	int getAIState();
	int getPatrolPoint();
	int updateAIState(Ogre::Vector3 targetPos, Ogre::Quaternion targetOrient);
	int seek(Ogre::Vector3 targetPos, Ogre::Quaternion targetOrient);
	int flee(Ogre::Vector3 targetPos, Ogre::Quaternion targetOrient);
	int patrol(Ogre::Vector3 targetPos, Ogre::Quaternion targetOrient);

	Ogre::SceneNode* waypoints[4];
	
	Ogre::SceneNode* engineNode1;
	Ogre::SceneNode* engineNode2;
	Ogre::SceneNode* engineTrailNode1;
	Ogre::SceneNode* engineTrailNode2;

	Ogre::BillboardSet* trailSet1;
	Ogre::BillboardSet* trailSet2;
	Ogre::RibbonTrail* engineTrail1;
	Ogre::RibbonTrail* engineTrail2;

protected:
	int currState;
	int currentWaypoint;
};

