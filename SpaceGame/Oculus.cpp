/*
 * Oculus.cpp integrates the Oculus OVR library with the SpaceGame project. Based on the OgreOculus implemention by 'Kojack'
 * Author : Alex Flight
 * Version : 5.0
 * Date : 03/05/2014
 *  v4.0  15/01/2014
 *  v3.0  04/12/2013
 *  v2.0  18/11/2013
 *  v1.0  14/11/2013
 */

#include "Oculus.h"

namespace
{
	// Default values for Oculus initialisation
	const float g_defaultNearClip = 0.01f;
	const float g_defaultFarClip = 50000.0f;
	const float g_defaultIPD = 0.064f;
	const Ogre::ColourValue g_defaultViewportColour(97/255.0f, 97/255.0f, 200/255.0f);
	const float g_defaultProjectionCentreOffset = 0.14529906f;
	const float g_defaultDistortion[4] = {1.0f, 0.22f, 0.24f, 0};
}

Oculus::Oculus(void) : mSensorFusion(0),
					   mStereoConfig(0),
					   mHmd(0),
					   mDeviceManager(0),
					   mOculusReady(false),
					   mOgreReady(false),
					   mSensor(0),
					   mCentreOffset(g_defaultProjectionCentreOffset),
					   mWindow(0),
					   mSceneManager(0),
					   mCameraNode(0)
{
	for(int i=0;i<2;++i)
	{
		mCameras[i] = 0;
		mViewports[i] = 0;
		mCompositors[i] = 0;
	}
}


Oculus::~Oculus(void)
{
	shutDownOculus();
}

// Disable and shut down the Oculus
void Oculus::shutDownOculus()
{
	mOculusReady = false;

	delete mStereoConfig;
	mStereoConfig = 0;
	delete mSensorFusion;
	mSensorFusion = 0;

	if(mSensor)
	{
		mSensor->Release();
	}
	if(mHmd)
	{
		mHmd->Release();
		mHmd = 0;
	}
	if(mDeviceManager)
	{
		mDeviceManager->Release();
		mDeviceManager = 0;
	}

	OVR::System::Destroy();
}

// Shut down Ogre Integration
void Oculus::shutDownOgre()
{
	mOgreReady = false;
	for(int i=0;i<2;++i)
	{
		if(mCompositors[i])
		{
			Ogre::CompositorManager::getSingleton().removeCompositor(mViewports[i], "Oculus");
			mCompositors[i] = 0;
		}
		if(mViewports[i])
		{
			mWindow->removeViewport(i);
			mViewports[i] = 0;
		}
		if(mCameras[i])
		{
			mCameras[i]->getParentSceneNode()->detachObject(mCameras[i]);
			mSceneManager->destroyCamera(mCameras[i]);
			mCameras[i] = 0;
		}
	}
	if(mCameraNode)
	{
		mCameraNode->getParentSceneNode()->removeChild(mCameraNode);
		mSceneManager->destroySceneNode(mCameraNode);
		mCameraNode = 0;
	}
	mWindow = 0;
	mSceneManager = 0;
}

bool Oculus::isOculusReady() const { return mOculusReady; }
bool Oculus::isOgreReady() const { return mOgreReady; }

// Set up all aspects of the Oculus device itself
bool Oculus::setupOculus()
{
	if(mOculusReady)
	{
		Ogre::LogManager::getSingleton().logMessage("Oculus - already initialised");
		return true;
	}
	// First init the DeviceManager
	Ogre::LogManager::getSingleton().logMessage("Oculus - initialising");
	OVR::System::Init(OVR::Log::ConfigureDefaultLog(OVR::LogMask_All));
	mDeviceManager = OVR::DeviceManager::Create();
	if(!mDeviceManager)
	{
		Ogre::LogManager::getSingleton().logMessage("Oculus - failed to initialise OVR Device Manager");
		return false;
	}
	// Next init the StereoConfig to enable value to be read from the Oculus
	Ogre::LogManager::getSingleton().logMessage("Oculus - initialised Device Manager");
	mStereoConfig = new OVR::Util::Render::StereoConfig();
	if(!mStereoConfig)
	{
		Ogre::LogManager::getSingleton().logMessage("Oculus - failed to initialise StereoConfig");
	}
	// Get the ProjectionCentreOffset and now init the HMDDevice representing the hardware itself
	mCentreOffset = mStereoConfig->GetProjectionCenterOffset();
	Ogre::LogManager::getSingleton().logMessage("Oculus - created SteroConfig");
	mHmd = mDeviceManager->EnumerateDevices<OVR::HMDDevice>().CreateDevice();
	if(!mHmd)
	{
		Ogre::LogManager::getSingleton().logMessage("Oculus - failed to initialise HMD");
		return false;
	}
	// Calibrate the device with the settings obtained directly from the HDMDevice
	Ogre::LogManager::getSingleton().logMessage("Oculus - created HMD");
	OVR::HMDInfo deviceInfo;
	mHmd->GetDeviceInfo(&deviceInfo);
	mStereoConfig->SetHMDInfo(deviceInfo);
	mSensor = mHmd->GetSensor();
	if(!mSensor)
	{
		Ogre::LogManager::getSingleton().logMessage("Oculus - failed to initialise Sensor");
		return false;
	}
	// Init SensorFusion object to manage sensor outputs
	Ogre::LogManager::getSingleton().logMessage("Oculus - created Sensor");
	mSensorFusion = new OVR::SensorFusion();
	if(!mSensorFusion)
	{
		Ogre::LogManager::getSingleton().logMessage("Oculus - failed to initialise SensorFusion");
		return false;
	}
	// Attach SensorFusion to actual sensor in the Oculus Device
	Ogre::LogManager::getSingleton().logMessage("Oculus - created SensorFusion");
	mSensorFusion->AttachToSensor(mSensor);
	mOculusReady = true;
	Ogre::LogManager::getSingleton().logMessage("Oculus - Oculus Setup Complete :: OCULUS READY");
	return true;
}

// After the Oculus is set up then call to integrate the device with Ogre
bool Oculus::setupOgre(Ogre::SceneManager *sceneManager, Ogre::RenderWindow *window, Ogre::SceneNode *parent)
{
	mWindow = window;
	mSceneManager = sceneManager;
	Ogre::LogManager::getSingleton().logMessage("Oculus - integrating with Ogre");
	// If a parent is passed to the Oculus, then use that as a parent SceneNode and inherit all orientation and movement
	if(parent)
	{
		mCameraNode = parent->createChildSceneNode("OculusStereoCameraNode");
		mCameraNode->setInheritOrientation(true);
		mCameraNode->setInheritScale(true);
	}
	// If a parent wasn't passed create a new SceneNode from the Root
	else
		mCameraNode = sceneManager->getRootSceneNode()->createChildSceneNode("OculusStereoCameraNode");

	// Init the two cameras for the Oculus
	mCameras[0] = sceneManager->createCamera("OculusCamLeft");
	mCameras[1] = sceneManager->createCamera("OculusCamRight");

	// Init the two ShaderMaterials ready for lens warping
	Ogre::MaterialPtr matLeft = Ogre::MaterialManager::getSingleton().getByName("Ogre/Compositor/Oculus");
	Ogre::MaterialPtr matRight = matLeft->clone("Ogre/Compositor/Oculus/Right");
	Ogre::GpuProgramParametersSharedPtr pParamsLeft = matLeft->getTechnique(0)->getPass(0)->getFragmentProgramParameters();
	Ogre::GpuProgramParametersSharedPtr pParamsRight = matRight->getTechnique(0)->getPass(0)->getFragmentProgramParameters();
	Ogre::Vector4 warp;

	// If the Oculus has passed its StereoConfig vars then use these to set up the warp parameters for distortion, else use the default values
	if(mStereoConfig)
	{
		warp = Ogre::Vector4(mStereoConfig->GetDistortionK(0), mStereoConfig->GetDistortionK(1), mStereoConfig->GetDistortionK(2), mStereoConfig->GetDistortionK(3));
	} else 
	{
		warp = Ogre::Vector4(g_defaultDistortion[0], g_defaultDistortion[1], g_defaultDistortion[2], g_defaultDistortion[3]);
	}

	// Integrate the warp params into the shader parameters
	pParamsLeft->setNamedConstant("HmdWarpParam", warp);
	pParamsRight->setNamedConstant("HmdWarpParam", warp);
	// Offset each lens centre to the left and right of the projection centre offset
	pParamsLeft->setNamedConstant("LensCentre", 0.5f+(mStereoConfig->GetProjectionCenterOffset()/2.0f));
	pParamsRight->setNamedConstant("LensCentre", 0.5f-(mStereoConfig->GetProjectionCenterOffset()/2.0f));

	// Create an Ogre Compositor for the warp shader and pass the pre-configured material to it
	Ogre::CompositorPtr compositor = Ogre::CompositorManager::getSingleton().getByName("OculusRight");
	compositor->getTechnique(0)->getOutputTargetPass()->getPass(0)->setMaterialName("Ogre/Compositor/Oculus/Right");

	// Set up both cameras in turn
	for(int i=0; i<2; ++i)
	{
		// Attach both cameras to the master CameraNode
		mCameraNode->attachObject(mCameras[i]);
		// Apply the vars from the StereoConfig directly to the camera settings, else use default values if not possible.
		if(mStereoConfig)
		{
			mCameras[i]->setNearClipDistance(mStereoConfig->GetEyeToScreenDistance());
			mCameras[i]->setFarClipDistance(g_defaultFarClip);
			mCameras[i]->setPosition((i*2-1) * mStereoConfig->GetIPD() * 0.5f, 0, 0);
			mCameras[i]->setAspectRatio(mStereoConfig->GetAspect());
			mCameras[i]->setFOVy(Ogre::Radian(mStereoConfig->GetYFOVRadians()));

			// Setup projection matrix correctly for the lens warp
			Ogre::Matrix4 proj = Ogre::Matrix4::IDENTITY;
			float temp = mStereoConfig->GetProjectionCenterOffset();
			proj.setTrans(Ogre::Vector3(-mStereoConfig->GetProjectionCenterOffset() * (2*i-1), 0, 0));
			mCameras[i]->setCustomProjectionMatrix(true, proj * mCameras[i]->getProjectionMatrix());
		} else
		{
			mCameras[i]->setNearClipDistance(g_defaultNearClip);
			mCameras[i]->setFarClipDistance(g_defaultFarClip);
			mCameras[i]->setPosition((i*2-1) * g_defaultIPD * 0.5f, 0, 0);
		}
		// Add a viewport for each camera and set up each Camera with the appropriate Compositor and shader.
		mViewports[i] = window->addViewport(mCameras[i], i, 0.5f*i, 0, 0.5f, 1.0f);
		mViewports[i]->setBackgroundColour(g_defaultViewportColour);
		mCompositors[i] = Ogre::CompositorManager::getSingleton().addCompositor(mViewports[i], i==0 ? "OculusLeft" : "OculusRight");
		mCompositors[i]->setEnabled(true);
	}
	// Ogre integration was successful and all output is configured to the Stereoscopic and warped requirements of the Rift
	mOgreReady = true;
	Ogre::LogManager::getSingleton().logMessage("Oculus - Oculus and Ogre set up!");
	return true;
}

// Move the Camera Orientation to reflect a change in position
void Oculus::updateCamera() 
{
	if(mOgreReady)
		mCameraNode->setOrientation(getOrientation());
}

Ogre::SceneNode *Oculus::getCameraNode()
{
	return mCameraNode;
}

// Return the Quaternion represenation the current Orientation of the Oculus itself based on the sensor output from the device. If no device return an Identity Quat
Ogre::Quaternion Oculus::getOrientation() const
{
	if(mOculusReady)
	{
		OVR::Quatf q = mSensorFusion->GetOrientation();
		return Ogre::Quaternion(q.w, q.x, q.y, q.z);
	} else
	{
		return Ogre::Quaternion::IDENTITY;
	}
}

// Return specified Oculus Compositor/shader
Ogre::CompositorInstance *Oculus::getCompositor(unsigned int i)
{
	return mCompositors[i];
}

float Oculus::getCentreOffset() const
{
	return mCentreOffset;
}

// Reset the Oculus sensor orientation values
void Oculus::resetOrientation()
{
	if(mSensorFusion)
		mSensorFusion->Reset();
}