/*
 * Celestial.h represents a Celestial Body in the game world, be it an asteroid, star or planet. A Celestial does not move and 
 * extends the basic GameEntity Class. Methods for generating procedural meshses and terrain are to be implemented as an extension.
 * Author : Alex Flight
 * Version : 2.0
 * Date : 11/03/2014
 *	v1.0  14/11/2013      
 */

#pragma once
#include "GameEntity.h"

class Celestial : public GameEntity
{
public:
	Celestial(Ogre::String entName, Ogre::String meshName, bool isDestroyable);
	~Celestial();

	void generateSphere();

protected:
};

