/*
 * Enemy.cpp represents an non-player controlled Enemy spacecraft entity within the game world. The class extends Spacecraft and introduces
 * a waypoint patrol system, an AI state system with pursue, patrol and flee states, and a series of nodes to incorporate ribbon trails
 * to allow for easier spotting of the Enemy against the background using its engine trails.
 * Author : Alex Flight
 * Version : 3.0
 * Date : 15/04/2014
	v2.0  23/03/2014
 *	v1.0  05/01/2014      
 */

#include "Enemy.h"

// Set up the default constructor for the Enemy and add waypoints for patrol path.
Enemy::Enemy() : Spacecraft("DEFAULT_Enemy", "enemyShip1.mesh", true, 2000, 50, 25, 5, 5, 5, 25, 15)
{
	currState = -1;
	currentWaypoint = 0;
	waypoints[0] = Engine::getSingletonPtr()->mSceneManager->getRootSceneNode()->createChildSceneNode("DEFAULT_Enemy_Waypoint1", Ogre::Vector3(0,0,-1000));
	waypoints[1] = Engine::getSingletonPtr()->mSceneManager->getRootSceneNode()->createChildSceneNode("DEFAULT_Enemy_Waypoint2", Ogre::Vector3(0,1000,0));
	waypoints[2] = Engine::getSingletonPtr()->mSceneManager->getRootSceneNode()->createChildSceneNode("DEFAULT_Enemy_Waypoint3", Ogre::Vector3(1000,0,0));
	waypoints[3] = Engine::getSingletonPtr()->mSceneManager->getRootSceneNode()->createChildSceneNode("DEFAULT_Enemy_Waypoint4", Ogre::Vector3(-500,-500,-500));

	setupEngineTrails();
}

/* 
 * Constructor for Enemy takes Spacecraft and GameEntity vars and sets up the waypoints and AI states of the Enemy. Every Enemy starts in an AI state of
 * -1 which represents a NULL state. Waypoints are added to a simple array of Ogre::SceneNodes.
 */
Enemy::Enemy(Ogre::String entName, Ogre::String meshName, bool isDestroyable, float objMass, float maxVel, float maxAccel, float roll, float pitch, float yaw,
					   float decceleration, float translate)
		: Spacecraft(entName, meshName, isDestroyable, objMass, maxVel, maxAccel, roll, pitch, yaw, decceleration, translate)
{
	currState = -1; //DEFAULT STATE
	currentWaypoint = 0;
	waypoints[0] = Engine::getSingletonPtr()->mSceneManager->getRootSceneNode()->createChildSceneNode(entName + "Waypoint1", Ogre::Vector3(0,0,-1000));
	waypoints[1] = Engine::getSingletonPtr()->mSceneManager->getRootSceneNode()->createChildSceneNode(entName + "Waypoint2", Ogre::Vector3(0,1000,0));
	waypoints[2] = Engine::getSingletonPtr()->mSceneManager->getRootSceneNode()->createChildSceneNode(entName + "Waypoint3", Ogre::Vector3(1000,0,0));
	waypoints[3] = Engine::getSingletonPtr()->mSceneManager->getRootSceneNode()->createChildSceneNode(entName + "Waypoint4", Ogre::Vector3(-500,-500,-500));

	setupEngineTrails();
}

// Default Destructor
Enemy::~Enemy()
{
}

// Call to create two nodes at the engine exhaust points on the Enemy mesh and create a simple Billboard RibbonTrail that displays an engine trail behind the 
// enemy as it moves through the world.
void Enemy::setupEngineTrails()
{
	engineNode1 = sceneNode->createChildSceneNode(Ogre::Vector3(-2,0,2));
	engineNode2 = sceneNode->createChildSceneNode(Ogre::Vector3(2,0,2));
	trailSet1 = sceneManager->createBillboardSet(1);
	trailSet1->createBillboard(Ogre::Vector3(0,0,0), Ogre::ColourValue(1,1,1,1));
	trailSet1->setMaterialName("Examples/Flare");
	trailSet1->setDefaultHeight(5.0);
	trailSet1->setDefaultWidth(5.0);
	engineNode1->attachObject(trailSet1);
	engineNode1->setVisible(true);

	trailSet2 = sceneManager->createBillboardSet(1);
	trailSet2->createBillboard(Ogre::Vector3(0,0,0), Ogre::ColourValue(1,1,1,1));
	trailSet2->setMaterialName("Examples/Flare");
	trailSet2->setDefaultHeight(3.0);
	trailSet2->setDefaultWidth(3.0);
	engineNode2->attachObject(trailSet2);
	engineNode2->setVisible(true);

	engineTrailNode1 = engineNode1->createChildSceneNode();
	engineTrailNode2 = engineNode2->createChildSceneNode();

	Ogre::NameValuePairList params;
	params["numberOfChains"] = "1";
    params["maxElements"] = "100";
    params["useTextureCoords"] = "true";
    params["useVertexColours"] = "true";

	engineTrail1 = (Ogre::RibbonTrail*)sceneManager->createMovableObject("RibbonTrail", &params);
	engineTrail1->setMaterialName("Examples/LightRibbonTrail");
	engineTrail1->setTrailLength(2000);
    engineTrail1->setInitialColour(0, 1, 1, 1, 1);
    engineTrail1->setColourChange(0, 0.5f, 0.5f, 0.5f, 2);
    engineTrail1->setInitialWidth(0, 40);
    engineTrail1->setWidthChange(0, 30);
    engineTrail1->addNode(engineTrailNode1);
	sceneManager->getRootSceneNode()->attachObject(engineTrail1);

	engineTrail2 = (Ogre::RibbonTrail*)sceneManager->createMovableObject("RibbonTrail", &params);
	engineTrail2->setMaterialName("Examples/LightRibbonTrail");
	engineTrail2->setTrailLength(2000);
    engineTrail2->setInitialColour(0, 1, 1, 1, 1);
    engineTrail2->setColourChange(0, 0.5f, 0.5f, 0.5f, 2);
    engineTrail2->setInitialWidth(0, 70);
    engineTrail2->setWidthChange(0, 60);
	engineTrail2->addNode(engineTrailNode2);
    sceneManager->getRootSceneNode()->attachObject(engineTrail2);
}

// Return the current enemy AI state
int Enemy::getAIState()
{
	return currState;
}

// Return current waypoint
int Enemy::getPatrolPoint()
{
	return currentWaypoint;
}

/*
 * updateAIState() method takes a target Vector3 and Quaternion and then switches AI state based on current Hull hit points as well as distance from the target
 * Vector3. If hull is too low attempt to steer away from the target (player) to flee from them, if they are within a certain range of the patrol path i.e.
 * "engagement range" then turn to pursue the target (player), else continue patrolling through the array of waypoints until next update.
 */
int Enemy::updateAIState(Ogre::Vector3 targetPos, Ogre::Quaternion targetOrient)
{
	int distanceToTarget = 0;
	if(hull < (MAX_HULL * .25f))
	{
		currState = 0; //FLEE STATE
		distanceToTarget = flee(targetPos, targetOrient);
	} if(sceneNode->getPosition().squaredDistance(targetPos) < 100000)
	{
		currState = 1; //SEEK STATE
		distanceToTarget = seek(targetPos, targetOrient);
	} else
	{
		currState = 2; //PATROL STATE/
		distanceToTarget = patrol(waypoints[currentWaypoint]->getPosition(), waypoints[currentWaypoint]->getOrientation());
	}
	return distanceToTarget;
}

/* 
 * Seek behaviour takes a target Vector3 and Quaternion and proceeds to calculate the direction and distance from the Enemy's current position and orientation.
 * With direction and position values calculated proceed to apply torque to turn in the direction of the target and apply thrust if the target is at a certain distance.
 */
int Enemy::seek(Ogre::Vector3 targetPos, Ogre::Quaternion targetOrient)
{
	Ogre::Vector3 currPos = sceneNode->getPosition();
	Ogre::Quaternion currOrient = sceneNode->getOrientation();
	Ogre::Vector3 vectorToTarget = targetPos - currPos;
	// Calculate Vector3 to the target then normalise
	currPos.normalise();
	currOrient.normalise();

	// Calculate headings/orientations
	Ogre::Vector3 heading = currOrient * currPos;
	Ogre::Vector3 targetHeading = targetOrient * targetPos;
	heading.normalise();
	targetHeading.normalise();

	// Multiply by unit vectors to get corrected orientations
	Ogre::Vector3 currVectorOrientation = currOrient.Inverse() * Ogre::Vector3::UNIT_Y;
	Ogre::Vector3 targetVectorOrientation = targetOrient * Ogre::Vector3::UNIT_Y;

	// Calculate torque vector as that which points straight towards the target and normlise
	Ogre::Vector3 torque = currOrient.Inverse() * vectorToTarget;
	torque.normalise();

	// CHEATING - MAKING ENEMY MOVEMENT WORK WITH SCENENODE MANIPULATION FOR NOW!
	/*sceneNode->pitch(Ogre::Radian(torque.y));
	sceneNode->yaw(Ogre::Radian(torque.x));
	sceneNode->roll(Ogre::Radian(targetVectorOrientation.getRotationTo(currVectorOrientation).getRoll().valueRadians()));

	if(sceneNode->getPosition().squaredDistance(targetPos) > 1000)
	{
		sceneNode->translate(Ogre::Vector3(0,0,10));
	} else if (sceneNode->getPosition().squaredDistance(targetPos) <= 500) 
	{
		sceneNode->translate(Ogre::Vector3(0,0,-10));
	} else 
	{
		sceneNode->translate(Ogre::Vector3(0,0,0));
	}*/

	// PHYSICS BASED MOVEMENT 
	// Input normalised torque and roll values into the physics controller of the Enemy
	pitch(torque.y);
	yaw(torque.x);
	roll(targetVectorOrientation.getRotationTo(currVectorOrientation).getRoll().valueRadians());
	
	// Check distance to target, if it is large then approach, else apply no thrust as Enemy is in range
	if(sceneNode->getPosition().squaredDistance(targetPos) > 100000)
	{
		thrust(1.0f);
	} else if (sceneNode->getPosition().squaredDistance(targetPos) <= 100000) 
	{
		thrust(0.0f);
	}
	// Return the squared distance to the target
	return sceneNode->getPosition().squaredDistance(targetPos);
}

// Fleet works the same as the seek() method except it applies steering in the opposite direction away from the target in order to avoid them.
int Enemy::flee(Ogre::Vector3 targetPos, Ogre::Quaternion targetOrient)
{
	Ogre::Vector3 currPos = sceneNode->getPosition();
	Ogre::Quaternion currOrient = sceneNode->getOrientation();
	Ogre::Vector3 vectorToTarget = targetPos - currPos;
	currPos.normalise();
	currOrient.normalise();

	Ogre::Vector3 heading = currOrient * currPos;
	Ogre::Vector3 targetHeading = targetOrient * targetPos;
	heading.normalise();
	targetHeading.normalise();

	Ogre::Vector3 currVectorOrientation = currOrient.Inverse() * Ogre::Vector3::UNIT_Y;
	Ogre::Vector3 targetVectorOrientation = targetOrient * Ogre::Vector3::UNIT_Y;

	// Steer away from the target Vector
	Ogre::Vector3 torque = currOrient * vectorToTarget;
	torque.normalise();

	// CHEATING - MAKING ENEMY MOVEMENT WORK WITH SCENENODE MANIPULATION FOR NOW!
	//sceneNode->pitch(Ogre::Radian(torque.y));
	//sceneNode->yaw(Ogre::Radian(torque.x));
	//sceneNode->roll(Ogre::Radian(targetVectorOrientation.getRotationTo(currVectorOrientation).getRoll().valueRadians()));

	//if(sceneNode->getPosition().squaredDistance(targetPos) > 100)
	//{
	//	sceneNode->translate(Ogre::Vector3(0,0,2));
	//} else if (sceneNode->getPosition().squaredDistance(targetPos) <= 100) 
	//{
	//	sceneNode->translate(Ogre::Vector3(0,0,0));
	//}

	// PHYSICS BASED MOVEMENT - NOT YET WORKING - COMING BACK TO IT IN LATER IMPLEMENTATIONS
	pitch(torque.y);
	yaw(torque.x);
	roll(targetVectorOrientation.getRotationTo(currVectorOrientation).getRoll().valueRadians());
	
	if(sceneNode->getPosition().squaredDistance(targetPos) > 100000)
	{
		thrust(1.0f);
	} else if (sceneNode->getPosition().squaredDistance(targetPos) <= 100000) 
	{
		thrust(0.0f);
	}
	return sceneNode->getPosition().squaredDistance(targetPos);
}

// Patrol works identically to seek() but increments waypoints when the current one is approached to within a certain range.
int Enemy::patrol(Ogre::Vector3 targetPos, Ogre::Quaternion targetOrient)
{
	Ogre::Vector3 currPos = sceneNode->getPosition();
	Ogre::Quaternion currOrient = sceneNode->getOrientation();
	Ogre::Vector3 vectorToTarget = targetPos - currPos;
	currPos.normalise();
	currOrient.normalise();

	Ogre::Vector3 heading = currOrient * currPos;
	Ogre::Vector3 targetHeading = targetOrient * targetPos;
	heading.normalise();
	targetHeading.normalise();

	Ogre::Vector3 currVectorOrientation = currOrient.Inverse() * Ogre::Vector3::UNIT_Y;
	Ogre::Vector3 targetVectorOrientation = targetOrient * Ogre::Vector3::UNIT_Y;

	Ogre::Vector3 torque = currOrient.Inverse() * vectorToTarget;
	torque.normalise();

	//CHEATING - MAKING ENEMY MOVEMENT WORK WITH SCENENODE MANIPULATION FOR NOW!
	//sceneNode->pitch(Ogre::Radian(torque.y));
	//sceneNode->yaw(Ogre::Radian(torque.x));
	//sceneNode->roll(Ogre::Radian(targetVectorOrientation.getRotationTo(currVectorOrientation).getRoll().valueRadians()));

	//if(sceneNode->getPosition().squaredDistance(targetPos) > 1000)
	//{
	//	sceneNode->translate(Ogre::Vector3(0,0,3));
	//} else if (sceneNode->getPosition().squaredDistance(targetPos) <= 1000) 
	//{
	//	if(currentWaypoint <= 3)
	//		currentWaypoint++;
	//	else
	//		currentWaypoint = 0;
	//	sceneNode->translate(Ogre::Vector3(0,0,0));
	//}

	//PHYSICS BASES MOVEMENT
	pitch(torque.y);
	yaw(torque.x);
	roll(targetVectorOrientation.getRotationTo(currVectorOrientation).getRoll().valueRadians());

	if(sceneNode->getPosition().squaredDistance(targetPos) > 100000 && torque.isZeroLength())
	{
		thrust(1.0f);
	} else if (sceneNode->getPosition().squaredDistance(targetPos) <= 100000) 
	{
		// Close enough to waypoint to consider it completed, increment and continue to next waypoint
		if(currentWaypoint <= 3)
			currentWaypoint++;
		else
			// Ran out of waypoints, reset to initial waypoint
			currentWaypoint = 0;
		thrust(0.0f);
	}
	return sceneNode->getPosition().squaredDistance(targetPos);
}