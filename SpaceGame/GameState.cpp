/*
 * GameState.cpp partially adapted from 'AdvancedOgreFramework'. Manages all of the game actions, inputs and objects
 * Author : Alex Flight
 * Version : 3.0
 * Date : 28/04/2014
 *  v2.0  14/01/2014
 *  v1.0  10/11/2013
 */
#include "GameState.h"

using namespace Ogre;

// Init GameState with all necessary elements
GameState::GameState()
{
	mLMouseDown = false;
	mRMouseDown = false;
	mQuit = false;
	mDetailsPanel = 0;
	mAlreadyInit = false;
	mSceneMgr = Engine::getSingletonPtr()->mSceneManager;
	physEngine = new Physics();
	physicsRate = 1.0f/60.0f;
	// Store vectors of Laser and Enemy object pointers
	laserList = new std::vector<Laser>;
	enemyList = new std::vector<Enemy*>;
}

void GameState::enter()
{
	Engine::getSingletonPtr()->mLog->logMessage("Entering GameState...");
	Engine::getSingletonPtr()->mTrayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);
	setupGUI();
	createScene(Engine::getSingletonPtr()->playerNode);
#ifdef OGRE_EXTERNAL_OVERLAY
	//mSceneMgr->addRenderQueueListener(Engine::getSingletonPtr()->mOverlaySystem);
#endif
}

bool GameState::pause()
{
	Engine::getSingletonPtr()->mLog->logMessage("\nPausing");
	return true;
}

void GameState::resume()
{
	Engine::getSingletonPtr()->mLog->logMessage("Resuming");
	setupGUI();
	//Engine::getSingletonPtr()->mViewport->setCamera(mCamera);
	mQuit = false;
}

void GameState::exit()
{
	Engine::getSingletonPtr()->mLog->logMessage("Leaving GameState");
#ifdef OGRE_EXTERNAL_OVERLAY
	mSceneMgr->removeRenderQueueListener(Engine::getSingletonPtr()->mOverlaySystem);
#endif
	//mSceneMgr->destroyCamera(mCamera);
	//if(mSceneMgr)
		//Engine::getSingletonPtr()->mRoot->destroySceneManager(mSceneMgr);
}

// Set up the game world and all objects within it
void GameState::createScene(Ogre::SceneNode* playerNode)
{
	//physEngine->setupDebugDrawer();
	// Check if objects have already been initialised in this instance of the application's runtime. If not create everything from scratch
	if(!mAlreadyInit)
	{
		// Create PlayerShip and attach the Physics Engine
		player = new PlayerShip("Player", "cockpit2.mesh", true, 1000, 500, 500, 5, 5, 5, 500, 250, physEngine);

		// Create an Enemy within the world
		testEnemy1 = new Enemy("Enemy1", "enemyShip1.mesh", true, 200, 500, 500, 5, 5, 5, 500, 250);
		//testEnemy2 = new Enemy("Enemy2", "enemyShip1.mesh", true, 200, 500, 500, 5, 5, 5, 500, 250);

		// Create a number of static asteroids for environmental detail/flavour
		asteroid1 = new Celestial("Asteroid1", "asteroid.mesh", false);
		asteroid2 = new Celestial("Asteroid2", "asteroid.mesh", false);
		asteroid3 = new Celestial("Asteroid3", "asteroid.mesh", false);
		asteroid4 = new Celestial("Asteroid4", "asteroid.mesh", false);
		asteroid5 = new Celestial("Asteroid5", "asteroid.mesh", false);
		asteroid6 = new Celestial("Asteroid6", "asteroid.mesh", false);

		// Add Enemy to the Enemy list
		enemyList->push_back(testEnemy1);

		// Postion initial spawn of Enemy
		btTransform trans;
		trans.setIdentity();
		trans.setOrigin(BtOgre::Convert::toBullet(Ogre::Vector3(0,0,-500)));
		testEnemy1->getMotionState()->setWorldTransform(trans);

		//trans.setOrigin(BtOgre::Convert::toBullet(Ogre::Vector3(0,300,-500)));
		//testEnemy2->getMotionState()->setWorldTransform(trans);

		// Add player to Scene and create a simple static light in the world
		mSceneMgr->getRootSceneNode()->removeChild("PlayerNode");
		player->getSceneNode()->addChild(playerNode);
		mSceneMgr->createLight("Light")->setPosition(10000,10000,10000);
		mSceneMgr->setAmbientLight(Ogre::ColourValue(0.2f, 0.2f, 0.2f));
	
		// Testing Ogre Head mesh to check lighting etc.
		//mOgreHeadEntity = mSceneMgr->createEntity("OgreHeadEntity", "ogrehead.mesh");
		//mOgreHeadNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("OgreHeadNode");
		//mOgreHeadNode->attachObject(mOgreHeadEntity);
		//mOgreHeadNode->setPosition(Vector3(0, 0, -205));

		//mOgreHeadMat = mOgreHeadEntity->getSubEntity(1)->getMaterial();
		//mOgreHeadMatHigh = mOgreHeadMat->clone("OgreHeadMatHigh");
		//mOgreHeadMatHigh->getTechnique(0)->getPass(0)->setAmbient(1, 0, 0);
		//mOgreHeadMatHigh->getTechnique(0)->getPass(0)->setDiffuse(1, 0, 0, 0);

		//Setup Space Dust Particles
		mParticleSystem = mSceneMgr->createParticleSystem("SpaceDust", "Space/Dust");
		SceneNode* cam = mSceneMgr->getSceneNode("OculusStereoCameraNode");
		particleNode = cam->createChildSceneNode("DustParticles", Engine::getSingletonPtr()->mCameraNode->getPosition());
		particleNode->attachObject(mParticleSystem);

		// Set up Laser BillboardSet
		laserNode = mSceneMgr->createSceneNode("Laser_Node");
		laserChain = mSceneMgr->createBillboardSet("laserChain");
		laserChain->setBillboardType(BBT_ORIENTED_COMMON);
		laserChain->setCommonDirection(Vector3::UNIT_Y);
		laserChain->setMaterialName("Examples/Flare");
		laserChain->setBillboardsInWorldSpace(true);
		laserChain->setBillboardOrigin(Ogre::BillboardOrigin::BBO_CENTER);
		laserChain->setDefaultDimensions(150, 150);
		laserNode->attachObject(laserChain);
		mSceneMgr->getRootSceneNode()->addChild(laserNode);
		Ogre::AxisAlignedBox aabBounds;
		aabBounds.setInfinite();
		laserChain->setBounds( aabBounds, Ogre::Math::POS_INFINITY );

		// Set up Gorilla GUI
		//Gorilla GUI REMOVED DUE TO UNRESOLVED COMPILER ERRORS
		/*gorillaSilverback = new Gorilla::Silverback();
		gorillaScreen = gorillaSilverback->createScreenRenderable(Ogre::Vector2(1,1), "world");
		gorillaNode = player->getSceneNode()->createChildSceneNode("Gorilla_NODE");
		gorillaNode->attachObject(gorillaScreen);
		gorillaUnderLayer = gorillaScreen->createLayer();
		gorillaOverLayer = gorillaScreen->createLayer();
		gorillaRect1 = gorillaUnderLayer->createRectangle(Ogre::Vector2::ZERO, Ogre::Vector2(100, 10));
		gorillaRect1->background_colour(Gorilla::Colours::Red);
		gorillaRect2 = gorillaUnderLayer->createRectangle(Ogre::Vector2::ZERO, Ogre::Vector2(10, 100));
		gorillaRect2->background_colour(Gorilla::Colours::Red);
		*/

	}

	// Set up Bullet Physics
	// NOW DONE EXTERNALLY!
	//bulBroadphase = new btDbvtBroadphase();
	//bulCollisionConfiguration = new btDefaultCollisionConfiguration();
	//bulDispatcher = new btCollisionDispatcher(bulCollisionConfiguration);
	//bulSolver = new btSequentialImpulseConstraintSolver;
	//bulDynamicsWorld = new btDiscreteDynamicsWorld(bulDispatcher, bulBroadphase, bulSolver, bulCollisionConfiguration);
	//bulDynamicsWorld->setGravity(btVector3(0,0,0));

	// Begin setting up physics engine integration
	//torque = btVector3(0,0,0);
	//shipRigidBody = player.getRigidBody();
	player->setupPhysics();
	//bulDynamicsWorld->addRigidBody(player.getRigidBody());
	physEngine->addRigidBody(player->getRigidBody());

	testEnemy1->getSceneNode()->setScale(10, 10, 10);
	testEnemy1->setupPhysics();
	testEnemy1->setPosition(Vector3(0,0, -500));
	//testEnemy2->getSceneNode()->setScale(10, 10, 10);
	//testEnemy2->setupPhysics();
	//testEnemy2->setPosition(Vector3(0,300, -500));
	//bulDynamicsWorld->addRigidBody(testEnemy1->getRigidBody());
	physEngine->addRigidBody(testEnemy1->getRigidBody());
	//physEngine->addRigidBody(testEnemy2->getRigidBody());

	// Put asteroids into position
	asteroid1->setPosition(Ogre::Vector3(-1000, 0, -300));
	asteroid1->getSceneNode()->setScale(5, 5, 5);
	asteroid2->setPosition(Ogre::Vector3(1000, -50, 50));
	asteroid2->getSceneNode()->setScale(5, 5, 5);
	asteroid3->setPosition(Ogre::Vector3(-1000, 300, 0));
	asteroid3->getSceneNode()->setScale(5, 5, 5);
	asteroid4->setPosition(Ogre::Vector3(500, -700, -100));
	asteroid4->getSceneNode()->setScale(5, 5, 5);
	asteroid5->setPosition(Ogre::Vector3(400, 500, -1000));
	asteroid5->getSceneNode()->setScale(5, 5, 5);
	asteroid6->setPosition(Ogre::Vector3(0, 0, 5000));
	asteroid6->getSceneNode()->setScale(5, 5, 5);

	// Init the game timer
	timer = new Timer();
	
	//mSceneMgr->setDisplaySceneNodes(true);
	
	// Declare game as initialised
	mAlreadyInit = true;
}

// Listen for key press events and act accordingly per keypress
bool GameState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
	switch(keyEventRef.key) {
		case(OIS::KC_ESCAPE):
			Engine::getSingletonPtr()->mLog->logMessage("ESC Pressed");
			pushState(findByName("PauseState"));
			break;
		case(OIS::KC_UP):
			player->pitch(-1.0f);
			//player.currPitch -= 10;
			break;
		case(OIS::KC_DOWN):
			player->pitch(1.0f);
			//player.currPitch += 10;
			break;
		case(OIS::KC_LEFT):
			player->yaw(1.0f);
			//player.currYaw += 10;
			break;
		case(OIS::KC_RIGHT):
			player->yaw(-1.0f);
			//player.currYaw -= 10;
			break;
		case(OIS::KC_Q):
			player->roll(1.0f);
			//player.currRoll += 10;
			break;
		case(OIS::KC_E):
			player->roll(-1.0f);
			//player.currRoll -= 10;
			break;
		case(OIS::KC_W):
			player->translateY(1.0f);
			//player.currTranslateY += 100;
			break;
		case(OIS::KC_S):
			player->translateY(-1.0f);
			//player.currTranslateY -= 100;
			break;
		case(OIS::KC_A):
			player->translateX(-1.0f);
			//player.currTranslateX -=100;
			break;
		case(OIS::KC_D):
			player->translateX(1.0f);
			//player.currTranslateX += 100;
			break;
		case(OIS::KC_LSHIFT):
			player->thrust(1.0f);
			//player.currTranslateZ -= 250;
			break;
		case(OIS::KC_LCONTROL):
			player->thrust(-1.0f);
			//player.currTranslateZ += 250;
			break;
		// TESTING ENEMY PHYSICS
		case(OIS::KC_I):
			testEnemy1->thrust(1.0f);
			break;
		case(OIS::KC_SPACE):
			player->isFiring(1);
			break;
		default:
			break;
	}
	return true;
}

// Listen for key release events
bool GameState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	Engine::getSingletonPtr()->keyPressed(keyEventRef);
		switch(keyEventRef.key) {
		case(OIS::KC_UP):
			player->pitch(0);
			//player.currPitch = 0;
			break;
		case(OIS::KC_DOWN):
			player->pitch(0);
			//player.currPitch = 0;
			break;
		case(OIS::KC_LEFT):
			player->yaw(0);
			//player.currYaw = 0;
			break;
		case(OIS::KC_RIGHT):
			player->yaw(0);
			//player.currYaw = 0;
			break;
		case(OIS::KC_Q):
			player->roll(0);
			//player.currRoll = 0;
			break;
		case(OIS::KC_E):
			player->roll(0);
			//player.currRoll = 0;
			break;
		case(OIS::KC_W):
			player->translateY(0);
			//player.currTranslateY = 0;
			break;
		case(OIS::KC_S):
			player->translateY(0);
			//player.currTranslateY = 0;
			break;
		case(OIS::KC_A):
			player->translateX(0);
			//player.currTranslateX = 0;
			break;
		case(OIS::KC_D):
			player->translateX(0);
			//player.currTranslateX = 0;
			break;
		case(OIS::KC_LSHIFT):
			player->thrust(0);
			//player.currTranslateZ = 0;
			break;
		case(OIS::KC_LCONTROL):
			player->thrust(0);
			//player.currTranslateZ = 0;
			break;
		case(OIS::KC_SPACE):
			player->isFiring(0);
			break;
		default:
			break;
	}
	return true;
}

// Listen for mouse moved events
bool GameState::mouseMoved(const OIS::MouseEvent &evt)
{
	if(Engine::getSingletonPtr()->mTrayMgr->injectMouseMove(evt)) return true;
	//shipRigidBody->applyTorque(btVector3(-evt.state.X.rel, evt.state.Y.rel, 0));
	return true;
}

// Listen for mouse pressed events
bool GameState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if(Engine::getSingletonPtr()->mTrayMgr->injectMouseDown(evt, id)) return true;

	if(id == OIS::MB_Left)
	{
		//onLeftPressed(evt);
		mLMouseDown = true;
	}
	else if(id == OIS::MB_Right)
	{
		mRMouseDown = true;
	}

	return true;
}

// Listen for mouse released events
bool GameState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if(Engine::getSingletonPtr()->mTrayMgr->injectMouseUp(evt, id)) return true;

	if(id == OIS::MB_Left)
	{
		mLMouseDown = false;
	}
	else if(id == OIS::MB_Right)
	{
		mRMouseDown = false;
	}
	return true;
}

// Deprecated method to receive player input from an external class
void GameState::getInput()
{
	
}

// Game update method called on every frame
void GameState::update(double timeSinceLastFrame)
{
	int distanceToTarget = 0;

	// Step frame rendering queue
	mFrameEvent.timeSinceLastFrame = timeSinceLastFrame;
	Engine::getSingletonPtr()->mTrayMgr->frameRenderingQueued(mFrameEvent);

	// Step physics
	float physicsTime = timeSinceLastFrame/1000.0f;
	int maxSteps = physicsTime/(physicsRate)+1;
	physEngine->stepSimulation(physicsTime, physicsRate);

	getInput();

	if(mQuit == true)
	{
		popState();
		return;
	}

	// Animate Space Dust
	spaceDust();
	
	// Apply player movement based on physics engine
	btTransform trans;
	player->getMotionState()->getWorldTransform(trans);
	player->getRigidBody()->activate(true);
	btMatrix3x3& movement = player->getRigidBody()->getWorldTransform().getBasis();
	btVector3 correctedTrans = movement * player->getTranslationVector();
	btVector3 correctedRot = movement * player->getRotationVector();
	player->getRigidBody()->applyTorque(player->getRigidBody()->getInvInertiaTensorWorld().inverse() * correctedRot * .1f);
	player->getRigidBody()->applyCentralImpulse(correctedTrans);
	player->update(timeSinceLastFrame, physEngine);
	// Fire player weapons if necessary
	if(player->getFiring()) {
		player->fireWeaponPrimary(laserChain, laserList, timeSinceLastFrame);
	}

	// UPDATE AI STATE
	distanceToTarget = testEnemy1->updateAIState(player->getSceneNode()->getPosition(), player->getSceneNode()->getOrientation());

	// Apply Enemy physics
	btTransform enemyTrans;
	testEnemy1->getMotionState()->getWorldTransform(enemyTrans);
	//btQuaternion enemyOrientation = enemyTrans.getRotation();
	testEnemy1->getRigidBody()->activate(true);
	btMatrix3x3& enemyMovement = testEnemy1->getRigidBody()->getWorldTransform().getBasis();
	btVector3 enemyCorrectedRot = enemyMovement * testEnemy1->getRotationVector();
	btVector3 enemyCorrectedTrans = enemyMovement * testEnemy1->getTranslationVector();
	testEnemy1->getRigidBody()->applyTorque(testEnemy1->getRigidBody()->getInvInertiaTensorWorld().inverse() * enemyCorrectedRot * .1f);
	testEnemy1->getRigidBody()->applyCentralImpulse(enemyCorrectedTrans);
	testEnemy1->update(timeSinceLastFrame, physEngine);

	// Update Lasers
	for (Laser l : *laserList) {
		l.update(timeSinceLastFrame, physEngine);
		// If laser raycast has hit something proceed to work out what
		/* Broken... calls to unreferenced pointer
		if(l.castRay(physEngine)->getUserPointer() != NULL)
		{
			// Cast an Ogre Entity to the CollisionObject created by the Ray Cast
			Ogre::Entity *entityHit = static_cast<Ogre::Entity*>(l.castRay(physEngine)->getUserPointer());
			if(entityHit != nullptr)
			{
				// Get hit name and check if it is an Enemy
				Ogre::String hitName = entityHit->getName();
				Ogre::String enemyName = "Enemy";
				// ERROR ray cast returns NULL POINTERS, need to fix to re-enable Laser hit detection 
				//if(hitName.find(enemyName) != Ogre::String::npos) {
				//	// Find the detected Enemy
				//	for(Enemy* e : *enemyList)
				//	{
				//		// If found then apply damage from the laser to the Enemy
				//		if(e->getName().find(hitName))
				//		{
				//			e->applyDamage(5);
				//			Engine::getSingletonPtr()->mLog->logMessage("DAMAGE APPLIED TO " + hitName);
				//		}
				//	}
				//}
			}
		}
		*/
	}

	// Output debug info
	//if(!Engine::getSingletonPtr()->mTrayMgr->isDialogVisible())
	//{
	//	if(mDetailsPanel->isVisible())
	//	{
	//		int currState = testEnemy1->getAIState();
	//		Ogre::String state;
	//		switch(currState)
	//		{
	//		case(0):
	//			state = "SEEK";
	//			Engine::getSingletonPtr()->mLog->logMessage("AI STATE - SEEK");
	//			break;
	//		case(1):
	//			state = "FLEE";
	//			Engine::getSingletonPtr()->mLog->logMessage("AI STATE - FLEE");
	//			break;
	//		default:
	//			state = "N/A";
	//			Engine::getSingletonPtr()->mLog->logMessage("AI STATE - NONE");
	//			break;
	//		}
	//		mDetailsPanel->setParamValue(0, Ogre::StringConverter::toString(testEnemy1->getTranslationVector().z()));
	//		mDetailsPanel->setParamValue(1, Ogre::StringConverter::toString(testEnemy1->getRotationVector().y()));
	//		mDetailsPanel->setParamValue(2, Ogre::StringConverter::toString(testEnemy1->getRotationVector().z()));
	//		mDetailsPanel->setParamValue(3, Ogre::StringConverter::toString(testEnemy1->getRotationVector().x()));
	//		mDetailsPanel->setParamValue(4, state);
	//	}
	//}

	// Periodically output debug info to log
	if(timer->getMilliseconds() > 2500)
	{
		Ogre::StringConverter* sc = new Ogre::StringConverter();
		// CHECKING STATES
		int currState = testEnemy1->getAIState();
		Ogre::String state;
		switch(currState)
		{
			case(0):
				state = "FLEE";
				Engine::getSingletonPtr()->mLog->logMessage("AI STATE " + state);
				break;
			case(1):
				state = "SEEK";
				Engine::getSingletonPtr()->mLog->logMessage("AI STATE " + state);
				break;
			case(2):
				state = "PATROL";
				Engine::getSingletonPtr()->mLog->logMessage("AI STATE " + state + "->" + sc->toString(testEnemy1->getPatrolPoint()));
			default:
				Engine::getSingletonPtr()->mLog->logMessage("DISTANCE TO TARGET ^2 " + sc->toString(distanceToTarget));
				break;
		}
		//CHECKING SCENENODES
		Engine::getSingletonPtr()->mLog->logMessage("PLAYER POS " + sc->toString(player->getPosition()));
		Engine::getSingletonPtr()->mLog->logMessage("ENEMY1 POS " + sc->toString(testEnemy1->getPosition()));

		Engine::getSingletonPtr()->mLog->logMessage("Player Torque " + sc->toString(player->getRotationVector().getX()) + " " + sc->toString(player->getRotationVector().getY()) + " " + sc->toString(player->getRotationVector().getZ()));
		Engine::getSingletonPtr()->mLog->logMessage("Player Trans " + sc->toString(player->getTranslationVector().getX()) + " " + sc->toString(player->getTranslationVector().getY()) + " " + sc->toString(player->getTranslationVector().getZ()));
		timer->reset();
	}
	// Update the player crosshair
	player->updateCrosshair();
}

// Particle system that enhances the feeling of movement to the player by moving small patricles past them as they move
void GameState::spaceDust()
{
	//Space Dust Particle System
	const float maxDist = 250;
	const float mirrorDist = maxDist*0.99;
	const double dimFactor = 0.8*0.005*0.005;
	Camera* cam = mSceneMgr->getCamera("OculusCamLeft");
	const Vector3& camPos = cam->getRealPosition();
	const float maxDist2 = 2*maxDist;
	
	ParticleIterator pit = mParticleSystem->_getIterator();
	while(!pit.end())
	{
		Particle* p = pit.getNext();
		Vector3& pos = p->position;
		p->timeToLive = 999999.0f;

		// position particles near camera
		// (keep moving them toward camera until within range)
		while (pos.x - camPos.x > maxDist)
			pos.x -= maxDist2;
		while (pos.x - camPos.x < -maxDist)
			pos.x += maxDist2;
		while (pos.y - camPos.y > maxDist)
			pos.y -= maxDist2;
		while (pos.y - camPos.y < -maxDist)
			pos.y += maxDist2;
		while (pos.z - camPos.z > maxDist)
			pos.z -= maxDist2;
		while (pos.z - camPos.z < -maxDist)
			pos.z += maxDist2;

		Vector3 pDir = pos - camPos;
		float dist = pDir.squaredLength();
		float dim = dist*dimFactor;
		p->setDimensions(dim, dim);
	}
}

// Setup the GUI
void GameState::setupGUI()
{
	Engine::getSingletonPtr()->mTrayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);

	Ogre::StringVector items;
	items.push_back("EnThrust");
	items.push_back("EnPitch");
	items.push_back("EnRoll");
	items.push_back("EnYaw");
	items.push_back("EnAISTATE");

	mDetailsPanel = Engine::getSingletonPtr()->mTrayMgr->createParamsPanel(OgreBites::TL_TOPLEFT, "AIDEBUG", 300, items);
	mDetailsPanel->show();
}