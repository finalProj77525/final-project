/*
 * Spacecraft.h class manages all Spacecraft Entities in the game world including gameplay logic, physics and movement
 * Author : Alex Flight
 * Version : 12.0
 * Date :   05/05/2014
 *	 v11.0  20/04/2014
 *   v10.0  05/04/2014
 *   v9.0   15/03/2014
 *   v8.0   12/03/2014
 *   v7.0   04/03/2014
 *   v6.0   18/02/2014
 *   v5.0   03/02/2014
 *   v4.0   20/01/2014
 *   v3.0   14/12/2013
 *   v2.0   28/11/2013
 *   v1.0   17/11/2013
 */

#include "Spacecraft.h"

// Constructor takes Entity name, mesh, destroyable flag, mass, max velocities and rotation/translation rates
Spacecraft::Spacecraft(Ogre::String entName, Ogre::String meshName, bool isDestroyable, float objMass, float maxVel, float maxAccel, float roll, float pitch, float yaw,
		float decceleration, float translate)
		: MoveableObject(entName, meshName, isDestroyable, objMass)
{
	MAX_ACCELERATION = maxAccel;
	MAX_VELOCITY = maxVel;
	ROLL_RATE = roll;
	PITCH_RATE = pitch;
	YAW_RATE = yaw;
	DECCELERATION_RATE = decceleration;
	TRANSLATE_RATE = translate;	
	laserID = 0;
	firing = false;
	laserCooldown = 0;
	//primaryWeapon = new Weapon(sceneNode, "PrimWep", 400);
	//luabind::module(Engine::getSingletonPtr()->pMyLuaState)
	//[
	//	luabind::class_<Spacecraft>("Spacecraft")
	//	.def("yaw", &Spacecraft::yaw)
	//];
}


Spacecraft::~Spacecraft()
{
	//delete shipRigidBody;
	//delete shape;
}

float Spacecraft::getShields()
{
	return shields;
}

float Spacecraft::getHull()
{
	return hull;
}

// Applies damage to the shields and then the hull of the Spacecraft and calculates the 'bleed through' if shields are not high enough to withstand the damage
void Spacecraft::applyDamage(float amount)
{
	if(amount > shields)
	{
		float bleedThrough = amount - shields;
		shields = 0;
		hull -= bleedThrough;
	} else 
	{
		shields -= amount;
	}
}

// Apply a normalised pitch rate (0.0 to 1.0) to change pitch by a a certain rate
void Spacecraft::pitch(float amount)
{
	currPitch = amount * PITCH_RATE;
}

// Apply a normalised roll rate (0.0 to 1.0) to change roll by a a certain rate
void Spacecraft::roll(float amount)
{
	currRoll = amount * ROLL_RATE;
}

// Apply a normalised yaw rate (0.0 to 1.0) to change yaw by a a certain rate
void Spacecraft::yaw(float amount)
{
	currYaw = amount * YAW_RATE;
}

// Apply a normalised thrust rate (0.0 to 1.0) to change thrust (z-acceleration) by a a certain rate
void Spacecraft::thrust(float amount)
{
	currTranslateZ = -amount * MAX_ACCELERATION;
}

// Apply a normalised translateX rate (0.0 to 1.0) to change X-acceleration
void Spacecraft::translateX(float amount)
{
	currTranslateX = amount * TRANSLATE_RATE;
}

// Apply a normalised translateY rate (0.0 to 1.0) to change Y-acceleration
void Spacecraft::translateY(float amount)
{
	currTranslateY = amount * TRANSLATE_RATE;
}

// Toggle Weapons fire
void Spacecraft::isFiring(int toggle) {
	if(toggle == 1) 
		firing = true;
	else
		firing = false;
}

bool Spacecraft::getFiring() {
	return firing;
}

// Fire the Primary Weapon by creating a new Laser object in the Laser list
void Spacecraft::fireWeaponPrimary(Ogre::BillboardSet* laserSet, std::vector<Laser>* lasers, double timeSinceLastFrame)
{
	//primaryWeapon->update(timeSinceLastFrame);
	if(laserCooldown <= 0) {
		new Laser(laserSet, sceneNode, lasers, 5, 10, 1000, 5000, "laser" + (laserID++));
		// Prevent refire for 50 ticks
		laserCooldown = 50;
	}
}

// Fire missile objects
void Spacecraft::fireWeaponSecondary()
{
	//TODO
}

btRigidBody* Spacecraft::getRigidBody()
{
	return shipRigidBody;
}

// Return the current Translation Vector3
btVector3 Spacecraft::getTranslationVector()
{
	return translation;
}

// Return the current Rotation Vector3
btVector3 Spacecraft::getRotationVector()
{
	return rotation;
}

// Setup the Spacecraft's Physics parameters
void Spacecraft::setupPhysics()
{
        btScalar bMass = MASS;
        btVector3 shipInertia;
        shape->calculateLocalInertia(bMass,shipInertia);

        btRigidBody::btRigidBodyConstructionInfo shipRigidBodyCI(bMass,motionState,shape,shipInertia);
        shipRigidBody = new btRigidBody(shipRigidBodyCI);
		// Activate a little Damping to prevent moving forever in a direction and to aid control simplicity
        shipRigidBody->setDamping(0.3,0.3);
        shipRigidBody->activate();
        currPitch = 0;
        currRoll = 0;
        currYaw = 0;
        currTranslateX = 0;
        currTranslateY = 0;
        currTranslateZ = 0;
        rotation = btVector3(0,0,0);
        translation = btVector3(0,0,0);
}

// Update the Spacecraft per frame
void Spacecraft::update(double timeSinceLastFrame, Physics* physEngine)
{
	// Decrement Laser Cooldown
	laserCooldown--;

	//primaryWeapon->update(timeSinceLastFrame);
	// Update rotation and translation vectors with current pitch, yaw, roll and translation values
	rotation = btVector3(currPitch, currYaw, currRoll);
	translation = btVector3(currTranslateX, currTranslateY, currTranslateZ);

	/*if(rotation.x() > 0)
		rotation.setX(rotation.x()-timeSinceLastFrame);
	if(rotation.x() < 0)
		rotation.setX(rotation.x()+timeSinceLastFrame);
	if(rotation.y() > 0)
		rotation.setY(rotation.y()-timeSinceLastFrame);
	if(rotation.y() < 0)
		rotation.setY(rotation.y()+timeSinceLastFrame);
	if(rotation.z() > 0)
		rotation.setZ(rotation.z()-timeSinceLastFrame);
	if(rotation.z() < 0)
		rotation.setZ(rotation.z()+timeSinceLastFrame);

	if(translation.x() > 0)
		translation.setX(translation.x()-timeSinceLastFrame);
	if(translation.x() < 0)
		translation.setX(translation.x()+timeSinceLastFrame);
	if(translation.y() > 0)
		translation.setY(translation.y()-timeSinceLastFrame);
	if(translation.y() < 0)
		translation.setY(translation.y()+timeSinceLastFrame);
	if(translation.z() > 0)
		translation.setZ(translation.z()-timeSinceLastFrame);
	if(translation.z() < 0)
		translation.setZ(translation.z()+timeSinceLastFrame);*/

	// Limit maximum velocity in any direction
	btVector3 velocity = shipRigidBody->getLinearVelocity();
	btScalar speed = velocity.length();
	if(speed > MAX_VELOCITY)
	{
		velocity *= MAX_VELOCITY/speed;
		shipRigidBody->setLinearVelocity(velocity);
	}

	// Iterate through the lasers belonging to this Spacecraft 
	for (auto & l : lasers)
	{
		if(l->update(timeSinceLastFrame, physEngine))
		{
			const btCollisionObject *object = l->castRay(physEngine);
			if(object != NULL)
			{
				// If a Spacecraft is hit, apply damage to it
				Spacecraft *hit = static_cast<Spacecraft*>(object->getUserPointer());
					if(hit->isAlive())
					{
						hit->applyDamage(20);
					}
				Engine::getSingletonPtr()->mLog->logMessage("Laser HIT");
			}
		}
	}
}