/*
 * PlayerShip.cpp class represents the player itself and adds beahviours only accessible to the Player themselves
 * Author : Alex Flight
 * Version : 3.0
 * Date : 23/04/2014
 *  v2.0  18/12/2013
 *  v1.0  17/11/2013
 */

#include "PlayerShip.h"

PlayerShip::PlayerShip(Ogre::String entName, Ogre::String meshName, bool isDestroyable, float objMass, float maxVel, float maxAccel, float roll, float pitch, float yaw,
					   float decceleration, float translate, Physics* engine)
		: Spacecraft(entName, meshName, isDestroyable, objMass, maxVel, maxAccel, roll, pitch, yaw, decceleration, translate)
{
	//primaryWeapon = static_cast<Weapon*>(Laser(engine, sceneNode, 50, 20, 400, 2000, "Laser1"));
	// Add a Crosshair Billboard is float in front of the camera in 3D
	
	crosshairNode = sceneNode->createChildSceneNode("CrosshairNode");
	Ogre::BillboardSet* crosshairSet = Engine::getSingletonPtr()->mSceneManager->createBillboardSet("crosshairSet");
	crosshairSet->setBillboardType(Ogre::BillboardType::BBT_ORIENTED_COMMON);
	crosshairSet->setCommonDirection(Ogre::Vector3::UNIT_Y);
	crosshairSet->setMaterialName("Crosshair");
	crosshairSet->setBillboardsInWorldSpace(true);
	crosshairSet->setBillboardOrigin(Ogre::BillboardOrigin::BBO_CENTER);
	crosshairSet->setDefaultDimensions(20, 20);
	crosshairNode->attachObject(crosshairSet);
	crosshairBB = crosshairSet->createBillboard(sceneNode->getPosition() + Ogre::Vector3(0, 20, -70));
	crosshairNode->setInheritOrientation(true);
}


PlayerShip::~PlayerShip()
{
}

void PlayerShip::targetObject()
{
	//TODO
}

void PlayerShip::toggleAdvFlightMode()
{
	//TODO
}

bool PlayerShip::keyPressed(const OIS::KeyEvent &keyEventRef)
{
	return true;
}

bool PlayerShip::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	Engine::getSingletonPtr()->keyPressed(keyEventRef);
	return true;
}

bool PlayerShip::mouseMoved(const OIS::MouseEvent &arg)
{
	return true;
}

bool PlayerShip::mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
	return true;
}

bool PlayerShip::mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
	return true;
}

// Update Crosshair position per frame, not working for Orientation at current time and remains at the nose of the Cockpit
void PlayerShip::updateCrosshair() {
	Ogre::Vector3 forward = Ogre::Vector3::NEGATIVE_UNIT_Z;
	Ogre::Vector3 direction = sceneNode->getOrientation() * forward;
	delete crosshairBB;
	crosshairBB = crosshairSet->createBillboard(sceneNode->getPosition() + direction + Ogre::Vector3(0, 20, -70));
	//crosshairBB->setPosition(sceneNode->getPosition() + direction + Ogre::Vector3(0, 20, -70));
}