/*
 * Physics.h integrates the Bullet Physics engine into the game
 * Author: Alex Flight
 * Version: 3.0
 * Date:  21/04/2014
 *  v2.0  20/01/2014
 *  v1.0  13/01/2014
 */

#pragma once
#include "Engine.h"
class Physics
{
public:
	Physics();
	~Physics();

	void addRigidBody(btRigidBody* body);
	void addCollisionObject(btCollisionObject* obj);
	void stepSimulation(double time, int subSteps);

	void setupDebugDrawer();

	btCollisionWorld* bulCollisionWorld;

protected:
	// Bullet Resources
	btBroadphaseInterface* bulBroadphase;
	btDefaultCollisionConfiguration* bulCollisionConfiguration;
	btCollisionDispatcher* bulDispatcher;
	btSequentialImpulseConstraintSolver* bulSolver;
	btDiscreteDynamicsWorld* bulDynamicsWorld;

	BtOgre::DebugDrawer* bulDebugDrawer;
};

