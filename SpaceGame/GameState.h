/*
 * GameState.h partially adapted from 'AdvancedOgreFramework'. Manages all of the game actions, inputs and objects
 * Author : Alex Flight
 * Version : 3.0
 * Date : 28/04/2014
 *  v2.0  14/01/2014
 *  v1.0  10/11/2013
 */
#ifndef GAME_STATE_H
#define GAME_STATE_H

#pragma once

#include "EngineState.h"
#include "PlayerShip.h"
#include "Enemy.h"
#include "Celestial.h"
#include <OgreSubEntity.h>
#include <OgreMaterialManager.h>


class GameState : public EngineState
{
public:
	GameState();
	DECLARE_ENGINESTATE_CLASS(GameState)

	void enter();
	void createScene(Ogre::SceneNode* playerNode);
	void exit();
	bool pause();
	void resume();

	void getInput();
	void setupGUI();

	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &arg);
	bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

	void update(double timeSinceLastFrame);
	void spaceDust();

private:
	Ogre::SceneNode* mOgreHeadNode;
	Ogre::Entity* mOgreHeadEntity;
	Ogre::MaterialPtr mOgreHeadMat;
	Ogre::MaterialPtr mOgreHeadMatHigh;
	OgreBites::ParamsPanel* mDetailsPanel;

	Ogre::Timer* timer;

	Ogre::ParticleSystem* mParticleSystem;
	Ogre::SceneNode* particleNode;

	Ogre::ParticleSystem* mSunParticleSystem;
	Ogre::SceneNode* mSunNode;

	Ogre::SceneNode* laserNode;
	Ogre::BillboardSet* laserChain;
	std::vector<Laser>* laserList;

	Physics* physEngine;

	PlayerShip* player;
	Enemy* testEnemy1;
	Enemy* testEnemy2;
	Celestial* asteroid1;
	Celestial* asteroid2;
	Celestial* asteroid3;
	Celestial* asteroid4;
	Celestial* asteroid5;
	Celestial* asteroid6;
	std::vector<Enemy*>* enemyList;

	Ogre::OverlayElement* debugElement;
	Ogre::TextAreaOverlayElement* mTextArea;

	//Gorilla GUI REMOVED DUE TO UNRESOLVED COMPILER ERRORS
	//Ogre::SceneNode* gorillaNode;
	//Gorilla::Silverback* gorillaSilverback;
	//Gorilla::ScreenRenderable* gorillaScreen;
	//Gorilla::Layer* gorillaOverLayer;
	//Gorilla::Layer* gorillaUnderLayer;
	//Gorilla::Rectangle* gorillaRect1;
	//Gorilla::Rectangle* gorillaRect2;

	bool mQuit;
	bool mLMouseDown, mRMouseDown;
	bool mAlreadyInit;

	float physicsRate;
};

#endif
