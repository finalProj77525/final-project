/*
 * Game.h intialises the game and Engine
 * Author : Alex Flight
 * Version : 1.0
 * Date : 11/11/2013
 */
#ifndef GAME_H
#define GAME_H

#pragma once

#include "Engine.h"
#include "EngineStateManager.h"

class Game
{
public:
	Game();
	~Game();

	void start();
private:
	EngineStateManager* mEngineStateManager;
};

#endif