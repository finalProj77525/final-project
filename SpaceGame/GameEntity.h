/*
 * GameEntity.h class represents every basic Entity within the game world with a mesh and SceneNode as well as methods for getting position, the SceneNode,
 * the Name and methods to destroy the object in the game.
 * Author : Alex Flight
 * Version : 1.0
 * Date : 17/11/2013
 */
#ifndef GAMEENTITY_H
#define GAMEENTITY_H

#pragma once

#include "Engine.h"

class GameEntity
{
public:
	GameEntity(Ogre::String entName, Ogre::String meshName, bool isDestroyable);
	~GameEntity();

	Ogre::Vector3 getPosition();
	Ogre::SceneNode* getSceneNode();
	Ogre::String getName();
	void setPosition(Ogre::Vector3 pos);
	bool destroy();
	bool isAlive();

protected:
	Ogre::SceneManager* sceneManager;
	Ogre::Entity* entity;
	Ogre::SceneNode* sceneNode;
	Ogre::Vector3 position;
	Ogre::String entityName;
	Ogre::String meshName;

    bool destroyable;
    bool alive;
};

#endif