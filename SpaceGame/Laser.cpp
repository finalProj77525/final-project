/*
 * Laser.h represents a Laser Billboard object that displays a laser projectile fired by the player. Additionally the Laser uses Ray Casts to determine hits
 * Author : Alex Flight
 * Version : 2.0
 * Date : 28/04/2014
 *  v1.0  15/03/2014
 */

#include "Laser.h"

Laser::Laser(Ogre::BillboardSet* laserChain, Ogre::SceneNode* parent, std::vector<Laser> *laserList, int width, int dmg, float rate, float life, Ogre::String name)
{
	LASERWIDTH = width;
	LASERDMG = dmg;
	lifetime = life;
	laserBillboard = laserChain->createBillboard(parent->getPosition() + Ogre::Vector3(0, 0, -50));
	Ogre::Vector3 forward = Ogre::Vector3::NEGATIVE_UNIT_Z;
	direction = parent->getOrientation() * forward;
	velocity = direction * 10;
	laserBillboard->setPosition(parent->getPosition());
	laserList->push_back(static_cast<Laser>(*this));
}


Laser::~Laser()
{
}

// Deprecated fire method, not used.
void Laser::fire()
{
	
}

// Return a CollisionObject pointer if a ray cast hits something.
const btCollisionObject* Laser::castRay(Physics* physEngine)
{
	Ogre::Vector3 from = laserBillboard->getPosition();
	Ogre::Vector3 to = laserBillboard->getPosition() * Ogre::Vector3(0,0,-LASERWIDTH);
	to = to + from;

	// Calculate to and from vectors (current position and position 1 frame in the future) to see if Laser will collide with a Physics Object.
	btVector3 btFrom = BtOgre::Convert::toBullet(from);
	btVector3 btTo = BtOgre::Convert::toBullet(to);
	btCollisionWorld::ClosestRayResultCallback rayCallback(btFrom, btTo);
	// Cast ray into the future position of the Laser
	physEngine->bulCollisionWorld->rayTest(btFrom, btTo, rayCallback);

	// Return a collision object if the Ray Cast hits something
	if(rayCallback.hasHit())
	{
		const btCollisionObject *object = rayCallback.m_collisionObject;
		return object;
	}
	return NULL;
}

// Update laser position per frame and decrement lifetime.
bool Laser::update(float timeSinceLastFrame, Physics* physEngine)
{
	Ogre::Vector3 position = laserBillboard->getPosition() + velocity * timeSinceLastFrame;
	//direction = direction + Ogre::Vector3(0,0,-20 * timeSinceLastFrame);
	laserBillboard->setPosition(position);
	//translate(0,0, -200 * timeSinceLastFrame);
	lifetime -= timeSinceLastFrame;
	return true;
}