Readme.txt
Alex Flight
FINAL PROJECT
Space Game for Oculus Rift

All source is included in the SpaceGame folder.

Compilation Instructions:

- Open the project solution in Visual Studio 2012 and make sure that the compilation target is 'Debug' for the 'x64' platform.

- Ensure the folder structure is preserved:
	- /Media
		- /materials
		- /models
		- /packs
		- /particle
		- /thumbnails
	- /SpaceGame
		- /Media...
		- /obj
		- /x64
			- /Debug
	- /x64
		- /Debug
		- /plugins_d_space_game.cfg
		- /resources_d_space_game.cfg
	- /OGRE.props
	- SpaceGame.sdf
	- SpaceGame.sln
	
- Copy the configuration options in the Visual Studio solution from the included screenshots

- Ensure the x64/Debug folder contains the libraries and .pdb files in the 'DLL1.png' & 'DLL2.png' screenshots

When compiling be sure to include the two Config files 'resources_d_space_game.cfg' and 'plugins_d_space_game.cfg' in the x64/Debug folder.
